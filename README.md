# AWS Container Service & PHP Example

For a talk I give that's a basic intro to ECS, Terraform, and Docker. This
particular flavor uses PHP as an example.

## Directory Structure

- `web`: The web root directory
- `docker`: Dockerfiles and configuration
- `infrastructure`: Terraform (and Ansible in a real project) modules, roles,
  and configuration.
- `bin` : Build Scripts

## How to use API

http://<servername>:<port>/?message=<Custome Mesage>

## How to Build
bin/build

## How to Deploy and Scale
 terraform apply
 
  bin/deploy in infra folder.
